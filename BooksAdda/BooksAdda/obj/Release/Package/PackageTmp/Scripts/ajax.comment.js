﻿$(function () {
    $("#commentpost").click(function () {
        var obj = {
            commentDescription: $(".typedcomment").val(),
            bookId: $("#BookId").val(),
            userEmail: $("#sessionName").val()
        }
        var ajaxRequest = makeAjaxCall("AddComment/Book", 'POST', JSON.stringify(obj), "json", "application/json");
        
        ajaxRequest.fail(function () {
            alert("error");
        });
        getComments();

    });  // end of commentpost    

    $("#viewcomments").click(function () { getComments() });
    $("#viewnextcomments").click(function () { getNextComments() });

    function getComments() {
        var x = $("#BookId").val();
        var ajaxRequest = makeAjaxCall("GetComments/Book", "GET", { bookId: x }, "json", "application/json");
        ajaxRequest.success(function (result) {
            $(".commentbox").css("display", "inline");
            var res = "";
            $.each(result, function (index, item) {
                res += '<p class="comment"><b>' + item.UserEmail + ' </b>' + item.CommentDescription + '</p>';
            });
            $(".displaycomment").html(res);

        });
        ajaxRequest.fail(function () {
            alert("error");
        });
    }

    // initialize page for comment
    var page = 1;
    function getNextComments() {
        var x = $("#BookId").val();
        //on each call,increment the page
        page = page + 1;
        var ajaxRequest = makeAjaxCall("ViewNextComments/Book", "GET", { bookId: x, page: page }, "json", "application/json");
        ajaxRequest.success(function (result) {
            $(".commentbox").css("display", "inline");
            var res = "";
            $.each(result, function (index, item) {
                res += '<p class="comment"><b>' + item.UserEmail + ' </b>' + item.CommentDescription + '</p>';
            });
            $(".displaycomment").html(res);

        });
        ajaxRequest.fail(function () {
            alert("error");
        });
    }

});


