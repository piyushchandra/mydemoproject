﻿function getSortedBooks() {
    var selectedSortProperty = jQuery("#SortBy option:selected").val();
    var ajaxRequest = makeAjaxCall("GetSortedBooks/Book", 'GET', { selectedSortProperty: selectedSortProperty }, "json", "application/json");

    ajaxRequest.fail(function () {
        alert("error");
    });
    ajaxRequest.success(function (result) {
        var res = "";
        debugger;
        $.each(result, function (index, item) {
            res += '<div class="box"><p><a href="GetBookById?BookId=' + item.BookId +
                '"><img class="book_img" src="' + item.ImageUrl + '" /></a></p><p class="book_info"><label>DiscountedPrice:</label>' +
                item.DiscountedPrice + '</p></div>';
        });
        $("#bookcontainer").html(res);
    });
}