﻿$(function () {
    var bookTags = [];
    var relatedBookIds = [];
    $(".tag-remove-btn").click(function () {        
        var bookTag = $(this).prev().attr("id");        
        bookTags.push(bookTag.toString());
        $(this).parent().hide();
    });

    $(".book-remove-btn").click(function () {
        var bookId = $(this).prev().attr("id");
        relatedBookIds.push(bookId.toString());
        $(this).parent().hide();
    });

    $("#editbook-update-btn").click(function () {

            var obj = { bookId: $("#BookId").val(), bookTags: bookTags };
            var ajaxRequest = makeAjaxCall("/admin/DeleteTags", 'POST', JSON.stringify(obj),
                "json", "application/json");        
    });
    $("#editbook-update-btn").click(function () {

        var obj = { bookId: $("#BookId").val(), relatedBookIds: relatedBookIds };
        var ajaxRequest = makeAjaxCall("/admin/DeleteRelatedBooks", 'POST',
            JSON.stringify(obj), "json", "application/json");
    });
});

$(function () {
    $("#relatedBookTitle").autocomplete({
        source: 'GetBookTitleJson'
    });
});
