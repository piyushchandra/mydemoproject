﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BooksAdda.CustomAttributes
{
    public class LesserThanAttribute : ValidationAttribute, IClientValidatable
    {
        public string PropertyNameToCompare { get; set; }

        public LesserThanAttribute(string propertyNameToCompare)
        {
            PropertyNameToCompare = propertyNameToCompare;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            return new ModelClientValidationRule[] { new ModelClientValidationRule { ValidationType = "lessthan", ErrorMessage = this.ErrorMessage } };
        }


        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var propertyToCompare = validationContext.ObjectType.GetProperty(PropertyNameToCompare);
            if (propertyToCompare == null)
            {
                return new ValidationResult(
                    string.Format("Invalid property name '{0}'", PropertyNameToCompare));
            }
            var valueToCompare = propertyToCompare.GetValue(validationContext.ObjectInstance, null);

            bool valid;

            if (value is short && valueToCompare is short)
            {
                valid = ((short)value) < ((short)valueToCompare);
            }
            //TODO: Other types
            else
            {
                return new ValidationResult("Compared properties should be numeric and of the same type.");
            }

            if (valid)
            {
                return ValidationResult.Success;
            }

            return new ValidationResult(
                string.Format("{0} must be lesser than {1}",
                    validationContext.DisplayName, PropertyNameToCompare));
        }
    }
}