﻿using BooksAdda.BusinessLayer;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BooksAdda.CustomAttributes
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        public string allowedroles;
        public string[] rolesarray;
        public CustomAuthorizeAttribute()
        {
            this.allowedroles = "";
        }

        public CustomAuthorizeAttribute(string roles)
        {
            this.allowedroles = roles;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool authorize = false;
            AuthorizationRepository repo = new AuthorizationRepository();
            rolesarray = allowedroles.Split(',');
            if (httpContext.Session["Name"] != null)
            {
                string userName = httpContext.Session["Name"].ToString();
                var roles = repo.GetRolesByUser(userName);
                foreach (var role in roles)
                {
                    if (rolesarray.Contains(role.RoleDescription))
                        authorize = true;
                }
            }
            return authorize;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
          //  filterContext.Result = new HttpUnauthorizedResult();
            if (filterContext.HttpContext.Session["Name"]== null)
            {
                filterContext.Result = new RedirectResult("~/Account/Login");
            }
            else
            {
                filterContext.Result = new RedirectResult("~/Error/NotAllowed");
            }
            
        }
    }
}