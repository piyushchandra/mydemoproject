﻿using BooksAdda.ViewModels;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace BooksAdda.BusinessLayer
{
    public class BooksRepository : ConnectionProvider
    {
        // Add a book
        public bool AddBook(BookViewModel model, string sellerEmail)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var addBookObject = new
                {
                    @Title = model.Title,
                    @Details = model.Details,
                    @PrintedPrice = model.PrintedPrice,
                    @DiscountedPrice = model.DiscountedPrice,
                    @SellerEmail = sellerEmail,
                    @ImageUrl = model.ImageUrl,
                    @Category = model.Category
                };
                var count = conn.Execute("book_create", addBookObject, commandType: CommandType.StoredProcedure);
                return count > 0;
            }
        }

        // Update a book
        public bool UpdateBook(BookViewModel model)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var updateBookObject = new
                {
                    @Title = model.Title,
                    @Details = model.Details,
                    @PrintedPrice = model.PrintedPrice,
                    @DiscountedPrice = model.DiscountedPrice,
                    @Category = model.Category,
                    @BookId = model.BookId
                };
                var count = conn.Execute("book_update", updateBookObject, commandType: CommandType.StoredProcedure);
                return count > 0;
            }
        }

        // Add comment to a book
        public bool AddComment(string commentDescription, int bookId, string userEmail)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var addCommentObj = new
                {
                    @CommentDescription = commentDescription,
                    @BookId = bookId,
                    @UserEmail = userEmail
                };
                var count = conn.Execute("comment_create", addCommentObj, commandType: CommandType.StoredProcedure);
                conn.Close();
                return count > 0;
            }
        }

        //Get all comments for a book
        public List<CommentViewModel> GetComments(int bookId)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var getCommentsObj = new { @BookId = bookId };
                List<CommentViewModel> commentsList = conn.Query<CommentViewModel>("get_comments", getCommentsObj,
                    commandType: CommandType.StoredProcedure).ToList();
                return commentsList;
            }
        }

        //Get Book List with category name
        public List<BookViewModel> GetBookListWithBookCategory()
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                List<BookViewModel> bookList = conn.Query<BookViewModel>("getbooks_with_category", commandType: CommandType.StoredProcedure).ToList();
                return bookList;
            }
        }

        // Get list of all categories for books
        public List<BookCategoryViewModel> GetBookCategoryList()
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                List<BookCategoryViewModel> categoryList = conn.Query<BookCategoryViewModel>("getbookscategory", commandType: CommandType.StoredProcedure).ToList();
                return categoryList;
            }
        }

        //Get list of book categories based on "searchTerm" match
        public List<BookCategoryViewModel> GetBookCategoryList(string searchTerm)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                List<BookCategoryViewModel> categoryList = conn.Query<BookCategoryViewModel>("getbookscategory_by_search",
                    new { @searchTerm = "%" + searchTerm + "%" }, commandType: CommandType.StoredProcedure).ToList();
                return categoryList;
            }
        }

        //Get list of books filtered by price and category
        public List<BookViewModel> GetFilteredBookListExceptEmail(BuyBooksViewModel model)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var getFilteredBookListExceptEmailobj = new
                {
                    @Email = model.LoggedInUserEmail,
                    @PriceMin = model.PriceMin,
                    @PriceMax = model.PriceMax,
                    @CategoryId = model.SelectedCategoryId
                };
                List<BookViewModel> filteredBookList = conn.Query<BookViewModel>("getfilteredbooks_except_loggedinuser", getFilteredBookListExceptEmailobj,
                    commandType: CommandType.StoredProcedure).ToList();
                return filteredBookList;
            }
        }

        // Get list of books added by all users except for logged in user
        public List<BookViewModel> GetBookListExceptEmail(string sellerEmail)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                List<BookViewModel> bookList = conn.Query<BookViewModel>("getbooks_except_email", new { @SellerEmail = sellerEmail }, commandType: CommandType.StoredProcedure).ToList();
                return bookList;
            }
        }

        // Get list of buyers for a book
        public List<string> GetBuyerListByBookId(int bookId)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                List<string> buyerList = conn.Query<string>("getbuyers_by_bookid",
                    new { @BookId = bookId }, commandType: CommandType.StoredProcedure).ToList();
                conn.Close();
                return buyerList;
            }
        }

        //Get book details by BookId
        public BookViewModel GetBookById(int id)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                BookViewModel book = conn.Query<BookViewModel>("getbook_by_id", new { @BookId = id },
                    commandType: CommandType.StoredProcedure).SingleOrDefault();
                return book;
            }
        }

        // Get list of books based on "searchText"
        public List<BookViewModel> SearchBooks(string searchText)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var books = conn.Query<BookViewModel>("book_search", new { @searchText = searchText }, commandType: CommandType.StoredProcedure).ToList();
                return books;
            }
        }

        // Get list of sorted books by selectedSortProperty
        public List<BookViewModel> GetSortedBooks(string selectedSortProperty)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var query = @"SELECT * FROM dbo.Book Order By ";
                var books = conn.Query<BookViewModel>(query + selectedSortProperty).ToList();
                return books;
            }
        }

        // Add related book to a book
        public bool AddRelatedBook(int bookId, string relatedBookTitle)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var addRelatedBookObj = new
                {
                    @BookId = bookId,
                    @RelatedBookTitle = relatedBookTitle
                };
                var count = conn.Execute("relatedbook_create", addRelatedBookObj, commandType: CommandType.StoredProcedure);
                return count > 0;
            }
        }

        // Add a tag to a book
        public bool AddBookTag(int bookId, string bookTag)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var addBookTagObj = new
                {
                    @BookId = bookId,
                    @BookTag = bookTag
                };
                var count = conn.Execute("[book_tag_create]", addBookTagObj, commandType: CommandType.StoredProcedure);
                return count > 0;
            }
        }

        // Get a list of related books
        public List<BookViewModel> GetRelatedBooks(int bookId)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var relatedBooks = conn.Query<BookViewModel>("get_related_books",
                    new { @BookId = bookId }, commandType: CommandType.StoredProcedure).ToList();
                return relatedBooks;
            }
        }

        // Get all tags for a book
        public List<string> GetTagsByBookId(int bookId)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var tags = conn.Query<string>("Select Tag from BookTag where BookId=@BookId",
                    new { @BookId = bookId }).ToList();
                return tags;
            }
        }

        // Get Book titles list
        public List<string> GetBookTitles(string term)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var titles = conn.Query<string>("Select Title from Book where Title LIKE @Term",
                    new { @Term = "%" + term + "%" }).ToList();
                return titles;
            }
        }

        // Delete a book
        public bool DeleteBookById(int id)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var count = conn.Execute("[book_delete_by_bookid]", new { @BookId = id },
                    commandType: CommandType.StoredProcedure);
                return count > 0;
            }
        }

        // Delete related books 
        public bool DeleteRelatedBooks(int bookId, int[] relatedBookIds)
        {
            DataTable tvp = new DataTable();
            using (var conn = GetConnection())
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("dbo.[relatedbooks_delete]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                tvp.Columns.Add("BookId", typeof(int));

                foreach (int id in relatedBookIds)
                {
                    tvp.Rows.Add(id);
                }
                SqlParameter param = new SqlParameter();
                SqlParameter param2 = new SqlParameter();
                param.ParameterName = "@list";
                param.Value = tvp;
                param.SqlDbType = SqlDbType.Structured;
                param2.ParameterName = "@BookId";
                param2.Value = bookId;
                cmd.Parameters.Add(param);
                cmd.Parameters.Add(param2);
                var count = cmd.ExecuteNonQuery();
                return count > 0;
            }
        }

        // Delete tags of a book
        public bool DeleteTags(int bookId, string[] bookTags)
        {
            DataTable tvp = new DataTable();
            using (var conn = GetConnection())
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("dbo.[book_tag_delete]", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                tvp.Columns.Add("Tag", typeof(string));
                tvp.Columns.Add("BookId", typeof(string));
                foreach (string tag in bookTags)
                {
                    tvp.Rows.Add(tag, bookId.ToString());
                }
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@list";
                param.Value = tvp;
                param.SqlDbType = SqlDbType.Structured;
                cmd.Parameters.Add(param);
                var count = cmd.ExecuteNonQuery();
                return count > 0;
            }
        }
    }
}