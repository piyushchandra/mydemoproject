﻿using BooksAdda.ViewModels;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BooksAdda.BusinessLayer
{
    public class NotificationRepository : ConnectionProvider
    {
        public void CreateNotification(int bookId, string buyerEmail, string sellerEmail)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var createNotificationObj = new
                {
                    @BookId = bookId,
                    @BuyerEmail = buyerEmail,
                    @SellerEmail = sellerEmail
                };
                conn.Execute("notification_create", createNotificationObj, commandType: CommandType.StoredProcedure);
            }
        }

        public void DeleteNotification(int bookId, string buyerEmail)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var deleteNotificationObj = new { @BookId = bookId, @BuyerEmail = buyerEmail };
                conn.Execute("notification_delete", deleteNotificationObj, commandType: CommandType.StoredProcedure);
            }
        }

        public List<NotificationViewModel> ListNotificationByEmail(string email)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                List<NotificationViewModel> notificationList = conn.Query<NotificationViewModel>
                    ("get_notification_by_email", new { @Email = email }, commandType: CommandType.StoredProcedure).ToList();
                return notificationList;
            }
        }
    }
}