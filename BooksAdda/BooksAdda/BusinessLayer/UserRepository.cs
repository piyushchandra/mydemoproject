﻿using BooksAdda.Encrypt;
using BooksAdda.ViewModels;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;


namespace BooksAdda.BusinessLayer
{
    public class UserRepository : ConnectionProvider
    {
        public void RegisterUser(BooksAdda.ViewModels.RegisterViewModel obj)
        {
            using (var conn = GetConnection())
            {
                string encryptedpass;
                Encyption encryption = new Encyption();
                encryptedpass = encryption.GetSHA1HashData(obj.Password);
                conn.Open();
                var registerUserObj = new
                {
                    @Name = obj.Name,
                    @Email = obj.Email,
                    @PhoneNo = obj.PhoneNo,
                    @Password = encryptedpass,
                    @RoleId = 14 // assign role=StandardUser while creating Account
                };
                conn.Execute("user_register", registerUserObj, commandType: CommandType.StoredProcedure);
            }
        }

        public bool IfUserExist(LoginViewModel model)
        {
            using (var conn = GetConnection())
            {
                string encryptedpass;
                Encyption encryption = new Encyption();
                encryptedpass = encryption.GetSHA1HashData(model.Password);
                conn.Open();
                var ifUserExistPara = new
                {
                    @Email = model.Email,
                    @Password = encryptedpass
                };
                var user = conn.Query<LoginViewModel>("is_user_exist", ifUserExistPara,
                    commandType: CommandType.StoredProcedure).SingleOrDefault();
                return (user != null);
            }
        }

        public bool IsAdmin(string email)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var user = conn.Query("is_admin", new { @UserEmail = email }, commandType: CommandType.StoredProcedure);
                var count = user.Count();
                return count > 0;
            }
        }

        public bool AddUser(UserViewModel userobject)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                Encyption encryption = new Encyption();
                string encryptedpass = encryption.GetSHA1HashData(userobject.Password);
                //fill User Table
                var addUserObj = new
                {
                    @Name = userobject.Name,
                    @Email = userobject.Email,
                    @PhoneNo = userobject.PhoneNo,
                    @Password = encryptedpass,
                    @Line1 = userobject.Line1,
                    @Area = userobject.Area,
                    @City = userobject.City,
                    @State = userobject.State,
                    @Pincode = userobject.Pincode,
                    @RoleId = 14 // assign role=StandardUser while creating Account
                };
                var count = conn.Execute("user_create", addUserObj, commandType: CommandType.StoredProcedure);
                return count > 0;
            }
        }

        public List<UserViewModel> GetUserList()
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                return conn.Query<UserViewModel>("getusers", commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public UserEditViewModel GetUserDetailsByEmail(string Email)
        {

            var conn = GetConnection();
            conn.Open();
            return conn.Query<UserEditViewModel>("getuser_with_address_by_email", new { @Email = Email },
                commandType: CommandType.StoredProcedure).SingleOrDefault();
        }

        public bool DeleteUser(string email)
        {
            var conn = GetConnection();
            conn.Open();
            //delete the user details
            var count = conn.Execute("user_delete", new { @Email = email }, commandType: CommandType.StoredProcedure);
            return count > 0;
        }

        public void UpdateUser(UserEditViewModel userobject)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var updateUserObj = new
                {
                    @Name = userobject.Name,
                    @Email = userobject.Email,
                    @PhoneNo = userobject.PhoneNo,
                    @Line1 = userobject.Line1,
                    @Area = userobject.Area,
                    @City = userobject.City,
                    @State = userobject.State,
                    @Pincode = userobject.Pincode
                };
                conn.Execute("user_update", updateUserObj, commandType: CommandType.StoredProcedure);
            }
        }

        public void CreateRecoveryPassword(string email, string id)
        {

            using (var conn = GetConnection())
            {
                conn.Open();
                //fill ResetPassword Table
                var createRecoveryPasswordObj = new
                {
                    @Id = id,
                    @Email = email
                };
                conn.Execute("create_recovery_password", createRecoveryPasswordObj, commandType: CommandType.StoredProcedure);
            }
        }

        public void DeleteRecoveryPassword(string email)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                //delete row from ResetPassword Table
                conn.Execute("delete_recovery_password", new { @Email = email }, commandType: CommandType.StoredProcedure);
            }
        }

        public string GetRecoveryEmailFromId(string id)
        {
            var conn = GetConnection();
            conn.Open();
            return conn.Query<string>("get_recovery_email_from_id", new { @Id = id }, commandType:
                CommandType.StoredProcedure).SingleOrDefault().ToString();
        }

        public void UpdatePassword(BooksAdda.ViewModels.ChangePasswordViewModel model)
        {
            using (var conn = GetConnection())
            {
                string encryptedpass;
                Encyption encryption = new Encyption();
                encryptedpass = encryption.GetSHA1HashData(model.Password);
                conn.Open();
                //Update User Table
                var updatePasswordObj = new
                {
                    @Password = encryptedpass,
                    @Email = model.Email
                };
                conn.Execute("update_new_password", updatePasswordObj, commandType: CommandType.StoredProcedure);

                //delete row from ResetPassword Table
                conn.Execute("delete_recovery_password", new { @Email = model.Email }, commandType: CommandType.StoredProcedure);
            }
        }

        public void AddUserFeedback(FeedbackViewModel model)
        {
            using (var conn = GetConnection())
            {
                var addUserFeedbackObj = new
                {
                    Email = model.Email,
                    Feedback = model.Comment
                };
                conn.Open();
                conn.Execute("feedback_create", addUserFeedbackObj, commandType: CommandType.StoredProcedure);
            }
        }
    }
}