﻿using BooksAdda.ViewModels;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BooksAdda.BusinessLayer
{
    public class AuthorizationRepository : ConnectionProvider
    {
        //Assign the role to user
        public bool AssignRole(int roleId, string userEmail)
        {            
            using (var conn = GetConnection())
            {
                conn.Open();
                var assignRoleObj = new
                {
                    @RoleId = roleId,
                    @UserEmail = userEmail
                };
                var count = conn.Execute("role_assign", assignRoleObj, commandType: CommandType.StoredProcedure);
                return (count > 0);
            }
        }

        // Get a list of Roles
        public List<RoleViewModel> GetRoleList()
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                List<RoleViewModel> role = conn.Query<RoleViewModel>("getroles", commandType: CommandType.StoredProcedure).ToList();
                return role;
            }
        }

        // Get list of Roles assigned to a user
        public List<RoleViewModel> GetRolesByUser(string userEmail)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                List<RoleViewModel> role = conn.Query<RoleViewModel>("getroles_by_email",
                    new { @UserEmail = userEmail }, commandType: CommandType.StoredProcedure).ToList();
                return role;
            }
        }

        //Add a new Role
        public bool AddRole(RoleViewModel model)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var count = conn.Execute("role_create", new { @RoleDescription = model.RoleDescription },
                    commandType: CommandType.StoredProcedure);
                return (count > 0);
            }
        }

        //Delete a Role
        public bool DeleteRole(RoleViewModel model)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var count = conn.Execute("role_delete", new { @RoleDescription = model.RoleDescription },
                    commandType: CommandType.StoredProcedure);
                return (count > 0);
            }
        }
    }
}