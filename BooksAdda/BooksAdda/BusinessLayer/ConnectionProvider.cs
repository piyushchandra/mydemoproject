﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

namespace BooksAdda.BusinessLayer
{
    public class ConnectionProvider
    {
        //Define the sql connection object.
        private SqlConnection Conn;   

        //Get Connection String
        protected SqlConnection GetConnection()
        {        
            try
            {
                var conectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                Conn = new SqlConnection(conectionString);
            }
            catch (Exception)
            {
                throw;
            }
            return Conn;
        }
    }
}