﻿function makeAjaxCall(serviceUrl, requestType, serviceData, dataType, contentType) {
    return $.ajax({
        type: requestType,
        url: serviceUrl,
        contentType: contentType,
        dataType: dataType, //Return type
        async: true,
        data: serviceData
    });
}

