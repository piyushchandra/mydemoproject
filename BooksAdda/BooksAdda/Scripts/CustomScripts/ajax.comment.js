﻿$(function () {
  
    getComments();
    $("#commentpost").click(function () {
        var obj = {
            commentDescription: $(".typedcomment").val(),
            bookId: $("#bookid").val(),
            userEmail: $("#session_name").val()
        }
        var ajaxRequest = makeAjaxCall("AddComment/Book", 'POST', JSON.stringify(obj), "json", "application/json");

        ajaxRequest.fail(function () {
            alert("error");
        });
        getComments();

    });  // end of commentpost    

    $("#viewpreviouscomments").click(function () { getPreviousComments() });
    $("#viewnextcomments").click(function () { getNextComments() });

    function getComments() {
        page = 1;
        var x = $("#bookid").val();
        var ajaxRequest = makeAjaxCall("GetComments/Book", "GET", { bookId: x }, "json", "application/json");
        ajaxRequest.success(function (result) {
            $(".commentbox").css("display", "inline");
            var res = "";
            $.each(result, function (index, item) {
                res += '<p class="comment"><b>' + item.UserEmail + ' </b>' + item.CommentDescription + '</p>';
            });
            $(".displaycomment").html(res);

        });
        ajaxRequest.fail(function () {
            alert("error");
        });
        setPager();
    }

    // initialize page for comment
    var page = 1;
    function getNextComments() {
        var x = $("#bookid").val();
        //on each call,increment the page
        page = page + 1;
        var ajaxRequest = makeAjaxCall("ViewNextComments/Book", "GET", { bookId: x, page: page }, "json", "application/json");
        ajaxRequest.success(function (result) {
            $(".commentbox").css("display", "inline");
            var res = "";
            $.each(result, function (index, item) {
                res += '<p class="comment"><b>' + item.UserEmail + ' </b>' + item.CommentDescription + '</p>';
            });
            $(".displaycomment").html(res);

        });
        ajaxRequest.fail(function () {
            alert("error");
        });
        setPager();
    }

    function getPreviousComments() {
        var x = $("#bookid").val();
        //on each call,increment the page
        page = page - 1;
        if (page < 1) {
            page = 1;
        }       
        var ajaxRequest = makeAjaxCall("ViewNextComments/Book", "GET", { bookId: x, page: page }, "json", "application/json");
        ajaxRequest.success(function (result) {
            $(".commentbox").css("display", "inline");
            var res = "";
            $.each(result, function (index, item) {
                res += '<p class="comment"><b>' + item.UserEmail + ' </b>' + item.CommentDescription + '</p>';
            });
            $(".displaycomment").html(res);

        });
        ajaxRequest.fail(function () {
            alert("error");
        });
        setPager();
    }

    function setPager()
    {
        if (page < 2) {
            $("#viewpreviouscomments").css("display", "none");
        }
        else {
            $("#viewpreviouscomments").css("display", "block");
        }
    }
});


