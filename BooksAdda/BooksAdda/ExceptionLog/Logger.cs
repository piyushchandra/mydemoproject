﻿using BooksAdda.DefinedConstants;
using System;
using System.IO;
using System.Web;

namespace BooksAdda.ExceptionLog
{
    public class Logger
    {
        public static void Log(Exception exc)
        {
            string LogFilePath = HttpContext.Current.Server.MapPath(ApplicationConstant.exceptionLogFilePath) +
                DateTime.Today.ToString("yyyyMMdd") + "." + "txt";

            FileStream logFileInfo = null;

            FileInfo logFilePathInfo = new FileInfo(LogFilePath);

            if (!logFilePathInfo.Exists)
            {
                logFileInfo = logFilePathInfo.Create();
            }
            else
            {
                logFileInfo = new FileStream(LogFilePath, FileMode.Append);
            }

            StreamWriter sw = new StreamWriter(logFileInfo);
            sw.WriteLine("********** {0} **********", DateTime.Now);
            if (exc.InnerException != null)
            {
                sw.Write("Inner Exception Type: ");
                sw.WriteLine(exc.InnerException.GetType().ToString());
                sw.Write("Inner Exception: ");
                sw.WriteLine(exc.InnerException.Message);
                sw.Write("Inner Source: ");
                sw.WriteLine(exc.InnerException.Source);
                if (exc.InnerException.StackTrace != null)
                {
                    sw.WriteLine("Inner Stack Trace: ");
                    sw.WriteLine(exc.InnerException.StackTrace);
                }
            }
            sw.Write("Exception Type: ");
            sw.WriteLine(exc.GetType().ToString());
            sw.WriteLine("Exception: " + exc.Message);
            sw.WriteLine("Source: " + exc.Source);
            sw.WriteLine("Stack Trace: ");
            if (exc.StackTrace != null)
            {
                sw.WriteLine(exc.StackTrace);
                sw.WriteLine();
            }
            sw.Close();
        }
    }
}