﻿namespace BooksAdda.DefinedConstants
{
    public static class ApplicationConstant
    {
        public const string imagesFilePath="/Images/";
        public const string smtpMailSendingEmail ="booksaddaforyou@gmail.com";
        public const string smtpMailSendingPassword = "8083044518";
        public const int commentPerPage=8;
        public const string exceptionLogFilePath = "/ExceptionLogFiles/";
        public const string userControllerAllowedRoles = "Admin,StandardUser";
        public const string bookControllerAllowedRoles = "Admin,StandardUser";
        public const string adminControllerAllowedRoles = "Admin";
        public const string notifyControllerAllowedRoles = "Admin,StandardUser";
        public const string utilityControllerAllowedRoles = "Admin,StandardUser";
    }
    

}