﻿using BooksAdda.BusinessLayer;
using BooksAdda.ExceptionLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace BooksAdda
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            System.Web.Optimization.PreApplicationStartCode.Start();
            BundleTable.EnableOptimizations = true;
        }


        protected void Application_Error(object Sender, EventArgs e)
       {
            Exception exc = Server.GetLastError();
            Logger.Log(exc);
            Server.ClearError();
            Response.Redirect("~/Error/ErrorMessage");


        }
    }
}
