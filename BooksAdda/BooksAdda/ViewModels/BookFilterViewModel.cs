﻿using System.Collections.Generic;

namespace BooksAdda.ViewModels
{
    public class BookFilterViewModel
    {
        public int PriceMax { get; set; }
        public int PriceMin { get; set; }
        public IEnumerable<BookViewModel> Books { get; set; }
    
    }
}