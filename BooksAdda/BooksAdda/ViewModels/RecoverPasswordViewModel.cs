﻿namespace BooksAdda.ViewModels
{
    public class RecoverPasswordViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}