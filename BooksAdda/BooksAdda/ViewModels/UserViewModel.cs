﻿using System.ComponentModel.DataAnnotations;

namespace BooksAdda.ViewModels
{
    public class UserViewModel
    {
        [Required]
        [Display(Name = "Name")]
        [StringLength(30, MinimumLength = 4, ErrorMessage = "Name must be between 4 and 30 characters")]
        public string Name { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email Address")]
        public string Email { get; set; }

        [Required]
        [RegularExpression(@"[1-9][0-9]{9}",ErrorMessage = "Please enter a 10 digit valid Phone Number.")]
        [Display(Name = "Phone Number")]
        public string PhoneNo { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "House/Street")]
        public string Line1 { get; set; }

        public string Area { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        [StringLength(6, ErrorMessage = "The Pincode number must be 6 characters long.")]
        public string Pincode { get; set; }

    }
}