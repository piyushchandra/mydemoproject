﻿namespace BooksAdda.ViewModels
{
    public class BookCategoryViewModel
    {
        public int Id { get; set; }
        public string Category { get; set; }
    }
}