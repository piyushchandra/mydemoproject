﻿using BooksAdda.Pagination;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BooksAdda.ViewModels
{
    public class BuyBooksViewModel
    {
        public List<BookViewModel> Books { get; set; }

        public Pager Pager { get; set; }

        public int? RequestedPage { get; set; }

        public List<BookCategoryViewModel> Categories { get; set; }

        public string LoggedInUserEmail { get; set; }

        // filtering data

        [Required(ErrorMessage="Please select a Category")]
        public int SelectedCategoryId { get; set; }

        [Required(ErrorMessage="Please select minimum price")]
        public int PriceMin { get; set; }

        [Required(ErrorMessage="Please select maximum price")]
        public int PriceMax { get; set; }

        [Display(Name = "Sort Books By")]
        public string SortBy { get; set; }

        public bool IsDeleted { get; set; }
    }
}