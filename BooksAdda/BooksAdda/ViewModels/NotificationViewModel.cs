﻿namespace BooksAdda.ViewModels
{
    public class NotificationViewModel
    {
        public int Id { get; set; }
        public int BookId { get; set; }
        public string BuyerEmail { get; set; }
        public string SellerEmail { get; set; }

    }
}