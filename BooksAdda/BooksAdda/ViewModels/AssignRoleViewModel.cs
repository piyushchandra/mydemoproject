﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BooksAdda.ViewModels
{
    public class AssignRoleViewModel
    {
        [Required(ErrorMessage = "User is required")]
        public string UserEmail { get; set; }

        public string RoleDescription { get; set; }

        public List<string> Emails { get; set; }

        public List<RoleViewModel> AvailableRoles { get; set; }

        public List<RoleViewModel> SelectedRoles { get; set; }

        [Required(ErrorMessage="Please select atleast one Role")]
        public PostedRoles PostedRoles { get; set; }

        public bool IsAssigned { get; set; }
    }
}