﻿namespace BooksAdda.ViewModels
{
    public class CommentViewModel
    {
        public int CommentId { get; set; }
        public int BookId { get; set; }
        public string CommentDescription { get; set; }
        public string UserEmail { get; set; }
    }
}