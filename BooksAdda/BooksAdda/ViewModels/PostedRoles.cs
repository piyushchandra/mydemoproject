﻿using System.Collections.Generic;

namespace BooksAdda.ViewModels
{
    public class PostedRoles
    {
        public List<int> RoleIds { get; set; }
        public string User { get; set; }
    }
}