﻿using System.ComponentModel.DataAnnotations;

namespace BooksAdda.ViewModels
{
    public class FeedbackViewModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        [Required(ErrorMessage="Please give your feedback")]
        public string Comment { get; set; }
    }
    
}