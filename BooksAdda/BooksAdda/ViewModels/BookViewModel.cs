﻿using BooksAdda.CustomAttributes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BooksAdda.ViewModels
{
    public class BookViewModel
    {
        [Required]
        [StringLength(50, MinimumLength = 4, ErrorMessage = "Title must be between 4 and 50 characters")]
        public string Title { get; set; }

        [Required]
        public string Category { get; set; }

        [StringLength(100, MinimumLength=4, ErrorMessage = "Details must be between 4 and 100 characters")]
        [Required]
        public string Details { get; set; }

        [Display(Name="Printed Price")]
        [Required]
        [RegularExpression(@"^[0-9]+$",ErrorMessage = "Invalid Printed Price")]
        public short PrintedPrice { get; set; }

        [Display(Name = "Discounted Price")]
        [Required]
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Invalid Printed Price")]
        [LesserThanAttribute("PrintedPrice")]
        public short DiscountedPrice { get; set; }

        public string ImageUrl { get; set; }

        public string SellerEmail { get; set; }

        public string BuyerEmail { get; set; }

        public List<string> BuyersEmail { get; set; }

        public List<BookViewModel> RelatedBooks { get; set; }

        public List<string> Tags { get; set; }

        [Display(Name = "Related Books")]
        public string relatedBookTitle { get; set; }

        [Display(Name = "Tags")]
        public string tagName { get; set; }

        public int BookId { get; set; }

        public bool IsAdded { get; set; }

        public bool IsDeleted { get; set; }
    }
}