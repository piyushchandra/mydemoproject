﻿using System.Collections.Generic;
using BooksAdda.Pagination;

namespace BooksAdda.ViewModels
{
    public class UserListViewModel
    {
        public List<UserViewModel> Users { get; set; }

        public Pager Pager { get; set; }

        public int? RequestedPage { get; set; }
    }
}