﻿using System.ComponentModel.DataAnnotations;

namespace BooksAdda.ViewModels
{
    public class UserEditViewModel
    {
        [Required]
        [Display(Name = "Name")]
        [StringLength(30, MinimumLength = 4, ErrorMessage = "Name must be between 4 and 30 characters")]
        public string Name { get; set; }

        [EmailAddress]
        [Display(Name = "Email Address")]
        public string Email { get; set; }

        [Required]
        [RegularExpression(@"[1-9][0-9]{9}", ErrorMessage = "Please enter a 10 digit valid Phone Number.")]
        [Display(Name = "Phone Number")]
        public string PhoneNo { get; set; }

        [Display(Name = "House/Street")]
        public string Line1 { get; set; }

        public string Area { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        [StringLength(6, ErrorMessage = "The Pincode number must be 6 characters long.")]
        public string Pincode { get; set; }

        public bool IsEditable { get; set; }
    }
}