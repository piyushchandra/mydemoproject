﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BooksAdda.ViewModels
{
    public class RoleViewModel
    {
        public string RoleId { get; set; }

        [Required(ErrorMessage="User Role is Required.")]
        [Display(Name="User Role")]
        public string RoleDescription { get; set; }

        public bool IsSelected { get; set; }  // for checkbox

        public List<string> Roles { get; set; }

        public bool IsAdded { get; set; }

        public bool IsDeleted { get; set; }
    }
}