﻿using BooksAdda.BusinessLayer;
using BooksAdda.CustomAttributes;
using BooksAdda.DefinedConstants;
using BooksAdda.ViewModels;
using System.Web.Mvc;

namespace BooksAdda.Controllers
{
    [CustomAuthorizeAttribute(allowedroles = ApplicationConstant.utilityControllerAllowedRoles)]
    public class UtilityController : Controller
    {
        public ViewResult Feedback()
        {
            FeedbackViewModel model = new FeedbackViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Feedback(FeedbackViewModel model)
        {
            if (ModelState.IsValid)
            {
                // Code for validating the CAPTCHA
                if (CaptchaMvc.HtmlHelpers.CaptchaHelper.IsCaptchaValid(this, "Captcha is not valid"))
                {
                    UserRepository repo = new UserRepository();
                    repo.AddUserFeedback(model);
                    return RedirectToAction("ThankYouPage");
                }
                else
                {
                    ViewBag.ErrMessage = "Error: captcha is not valid.";
                }
            }
            return View(model);
        }

        public ViewResult ThankYouPage()
        {
            return View();
        }
    }
}