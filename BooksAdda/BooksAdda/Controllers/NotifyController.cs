﻿using BooksAdda.BusinessLayer;
using BooksAdda.CustomAttributes;
using BooksAdda.DefinedConstants;
using BooksAdda.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace BooksAdda.Controllers
{
    [CustomAuthorizeAttribute(allowedroles = ApplicationConstant.notifyControllerAllowedRoles)]
    public class NotifyController : Controller
    {
        public ActionResult CreateNotification(int bookId, string buyerEmail, string sellerEmail)
        {
            NotificationRepository repo = new NotificationRepository();
            repo.CreateNotification(bookId, buyerEmail, sellerEmail);
            return RedirectToAction("GetBookById", "Book", new { BookId = bookId });
        }

        public ActionResult DeleteNotification(int bookId, string buyerEmail)
        {
            NotificationRepository repo = new NotificationRepository();
            repo.DeleteNotification(bookId, buyerEmail);
            return RedirectToAction("GetBookById", "Book", new { bookId = bookId });

        }
        public ViewResult ShowNotificationByEmail(string email)
        {
            NotificationRepository repo = new NotificationRepository();
            List<NotificationViewModel> notificationList = repo.ListNotificationByEmail(email);
            return View(notificationList);
        }
    }
}