﻿using BooksAdda.BusinessLayer;
using BooksAdda.CustomAttributes;
using BooksAdda.DefinedConstants;
using BooksAdda.ViewModels;
using System;
using System.Web.Mvc;

namespace BooksAdda.Controllers
{
    [CustomAuthorizeAttribute(allowedroles = ApplicationConstant.userControllerAllowedRoles)]
    public class UserController : Controller
    {
        public ViewResult AddUsers() // Work while we request page  
        {
            return View();
        }

        [HttpPost]
        public ViewResult AddUsers(UserViewModel userViewModel) // Work while we post page  
        {
            UserRepository repo = new UserRepository();
            if (ModelState.IsValid)
            {
                if (repo.AddUser(userViewModel))
                {
                    ViewBag.UserAddedMessage = userViewModel.Name + " " + "was Added Successfully.";
                }
                ModelState.Clear();
            }
            return View(userViewModel);
        }

        public ViewResult GetUserByEmail(string email) // Work while we request page  
        {
            UserRepository repo = new UserRepository();
            UserEditViewModel user = repo.GetUserDetailsByEmail(email);
            ViewBag.EditMessage = TempData["EditMessage"];
            return View(user);
        }

        [HttpPost]
        public ActionResult EditUsers(UserEditViewModel userViewModel) // Work while we request page  
        {
            UserRepository repo = new UserRepository();
            if (ModelState.IsValid)
            {
                try
                {
                    repo.UpdateUser(userViewModel);
                    ViewBag.EditMessage = "Hi" + " " + userViewModel.Name + ". " + "Your Account has been Updated";
                    ModelState.Clear();
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Invalid Update");
                }
            }
            return View("GetUserByEmail", userViewModel);
        }
    }
}