﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BooksAdda.BusinessLayer;
using BooksAdda.CustomAttributes;
using BooksAdda.Pagination;
using BooksAdda.ViewModels;
using System.Collections.Generic;
using BooksAdda.DefinedConstants;


namespace BooksAdda.Controllers
{
    [CustomAuthorizeAttribute(allowedroles = ApplicationConstant.bookControllerAllowedRoles)]
    public class BookController : Controller
    {
        [HttpGet]
        public ViewResult AddBooks()
        {
            BooksRepository repo = new BooksRepository();
            ViewBag.IsAdded = false;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ViewResult AddBooks(BookViewModel model, HttpPostedFileBase file) // Work while we post page
        {
            BooksRepository repo = new BooksRepository();
            if (ModelState.IsValid)
            {
                //upload book's image
                if (file != null && file.ContentLength > 0)
                {
                    try
                    {
                        string path = Path.Combine(HttpContext.Server.MapPath(ApplicationConstant.imagesFilePath),
                         Path.GetFileName(file.FileName));
                        file.SaveAs(path);
                        model.ImageUrl = ApplicationConstant.imagesFilePath + Path.GetFileName(file.FileName);
                        var email = Session["Name"].ToString();
                        ViewBag.IsAdded = repo.AddBook(model, email);
                        ViewBag.FileMessage = "Book was uploaded successfully";
                        ModelState.Clear();
                        model = new BookViewModel();
                    }
                    catch (Exception)
                    {
                        ModelState.AddModelError("", "Sorry,some error occured while Adding the Book.");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "File invalid");
                    ViewBag.FileMessage = "You have not specified a file.";
                }
            }
            ViewBag.IsAdded = false;
            return View(model);
        }

        public ActionResult UpdateBooks(BookViewModel model)
        {
            BooksRepository repo = new BooksRepository();
            repo.UpdateBook(model);
            if (model.BookId > 0 && model.relatedBookTitle != null)
            {
                repo.AddRelatedBook(model.BookId, model.relatedBookTitle);
            }

            if (model.BookId > 0 && model.tagName != null)
            {
                repo.AddBookTag(model.BookId, model.tagName);
            }
            return RedirectToAction("BookEditById", "Admin", new { bookId = model.BookId });
        }

        public ViewResult BuyBooks(int? page)
        {
            BooksRepository repo = new BooksRepository();
            BuyBooksViewModel model = new BuyBooksViewModel();
            model.Categories = repo.GetBookCategoryList();
            model.RequestedPage = page;
            if (Session["Name"] != null)
            {
                model.Books = repo.GetBookListExceptEmail(Session["Name"].ToString());
                SetPager(model);
            }
            return View(model);
        }

        [HttpPost]
        public ViewResult BuyBooks(BuyBooksViewModel model)
        {
            BooksRepository repo = new BooksRepository();
            model.Categories = repo.GetBookCategoryList();
            if (Session["Name"] != null)
            {
                model.LoggedInUserEmail = Session["Name"].ToString();
                model.Books = repo.GetFilteredBookListExceptEmail(model);
                SetPager(model);
            }
            return View(model);
        }

        // sets the pager values according to model properties
        public BuyBooksViewModel SetPager(BuyBooksViewModel model)
        {
            var pager = new Pager(model.Books.Count(), model.RequestedPage);
            model.Books = model.Books.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize).ToList();
            model.Pager = pager;
            return model;
        }

        [AllowAnonymous]
        public ViewResult ListBooks(int? page)
        {
            BooksRepository repo = new BooksRepository();
            BuyBooksViewModel model = new BuyBooksViewModel();
            model.Books = repo.GetBookListWithBookCategory();
            model.RequestedPage = page;
            SetPager(model);
            return View(model);
        }

        public ViewResult GetBookById(int bookId)
        {
            BooksRepository repo = new BooksRepository();
            BookViewModel model = repo.GetBookById(bookId);
            model.BuyersEmail = repo.GetBuyerListByBookId(bookId);
            model.RelatedBooks = repo.GetRelatedBooks(bookId);
            model.Tags = repo.GetTagsByBookId(bookId);
            return View(model);
        }

        //get book categories according to paramenter
        public JsonResult GetBookCategoryJson(string term)
        {
            BooksRepository repo = new BooksRepository();
            List<BookCategoryViewModel> categories;
            List<string> list;
            if (String.IsNullOrEmpty(term))
            {
                categories = repo.GetBookCategoryList();
            }
            else
            {
                categories = repo.GetBookCategoryList(term);
            }
            list = categories.Select(y => y.Category).ToList();
            return new JsonResult { Data = list, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetComments(int bookId)
        {
            BooksRepository repo = new BooksRepository();
            List<CommentViewModel> comments = repo.GetComments(bookId);
            comments = comments.Take(8).ToList();
            return new JsonResult { Data = comments, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult ViewNextComments(int bookId, int page)
        {
            BooksRepository repo = new BooksRepository();
            List<CommentViewModel> comments = repo.GetComments(bookId);
            // set pagination for comment
            int totalPages = (int)Math.Ceiling((comments.Count) / (double)ApplicationConstant.commentPerPage);
            if (page < 1 || page > totalPages)
                page = 1;
            comments = comments.Skip(ApplicationConstant.commentPerPage * (page - 1)).
                Take(ApplicationConstant.commentPerPage).ToList();
            return new JsonResult { Data = comments, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public JsonResult AddComment(string commentDescription, int bookId, string userEmail)
        {
            BooksRepository repo = new BooksRepository();
            repo.AddComment(commentDescription, bookId, userEmail);
            return Json(new { success = true });
        }

        [HttpPost]
        public ViewResult SearchBooks(string searchText)
        {
            BooksRepository repo = new BooksRepository();
            BuyBooksViewModel model = new BuyBooksViewModel();
            model.Categories = repo.GetBookCategoryList();
            model.Books = repo.SearchBooks(searchText);
            SetPager(model);
            return View("BuyBooks", model); // return the same view used for BuyBooks
        }

        public JsonResult GetSortedBooks(string selectedSortProperty)
        {
            BooksRepository repo = new BooksRepository();
            var books = repo.GetSortedBooks(selectedSortProperty);
            return new JsonResult { Data = books, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}