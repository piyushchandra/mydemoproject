﻿using BooksAdda.BusinessLayer;
using BooksAdda.DefinedConstants;
using BooksAdda.ViewModels;
using System;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;

namespace BooksAdda.Controllers
{
    public class AccountController : Controller
    {
        #region
        // public methods
        public ViewResult Homepage()
        {
            return View();
        }

        public ViewResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (CaptchaMvc.HtmlHelpers.CaptchaHelper.IsCaptchaValid(this, "Captcha is not valid"))
                {
                    UserRepository repo = new UserRepository();
                    try
                    {
                        repo.RegisterUser(model);
                    }
                    catch (Exception)
                    {
                        ModelState.AddModelError("", "Sorry,Some Exception Occured ");
                        return View();
                    }
                    TempData["RegistrationMessage"] = @"Hi " + model.Name + "! " + "You are successfully Registered." +
                                                              "Please Login to Continue";
                    ModelState.Clear();
                    return RedirectToAction("Login");
                }
                else
                {
                    ViewBag.ErrMessage = "Error: captcha is not valid.";
                }
            }
            return View(model);
        }

        public ViewResult Login()
        {
            ViewBag.RegistrationMessage = TempData["RegistrationMessage"];
            ViewBag.PasswordChangeMessage = TempData["PasswordChange"];
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ViewResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                UserRepository repo = new UserRepository();
                var userexist = repo.IfUserExist(model);
                if (userexist)
                {
                    Session["Name"] = model.Email.ToString();
                    var isAdmin = repo.IsAdmin(model.Email);
                    if (isAdmin)
                        Session["IsAdmin"] = "true";
                    return View("Homepage");
                }
                else
                {
                    ModelState.AddModelError("", "Invalid Email or Password");
                }
            }
            return View(model);
        }

        public ActionResult LogOut()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Homepage", "Account");
        }

        public ViewResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ViewResult ForgotPassword(UserViewModel model)
        {
            Guid UniqueId = Guid.NewGuid();
            //save to database
            UserRepository repo = new UserRepository();
            repo.CreateRecoveryPassword(model.Email, UniqueId.ToString());
            SendPasswordResetEmail(model.Email, UniqueId.ToString());
            return View("RecoverPassword", model);
        }

        public ViewResult ChangePassword(string id)
        {
            UserRepository repo = new UserRepository();
            string email = repo.GetRecoveryEmailFromId(id);
            if (email != null)
            {
                ChangePasswordViewModel model = new ChangePasswordViewModel();
                model.Email = email;
                return View(model);
            }
            return View("Error");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            UserRepository repo = new UserRepository();
            repo.UpdatePassword(model);
            repo.DeleteRecoveryPassword(model.Email);
            TempData["PasswordChange"] = "Password was changed successfully.";
            return RedirectToAction("Login");
        }

        // end of public methods
        #endregion
        

        #region 
        // private methods
        private void SendPasswordResetEmail(string email, string uniqueId)
        {
            MailMessage mailmessage = new MailMessage(ApplicationConstant.smtpMailSendingEmail, email);
            StringBuilder sbEmailBody = new StringBuilder();
            sbEmailBody.Append("Please click on the link to change password" + "<br/><br/>");
            var absoluteUri = Request.Url.AbsoluteUri;
            var absolutePath = Request.Url.AbsolutePath;
            absoluteUri = absoluteUri.Substring(0, absoluteUri.Length - absolutePath.Length);
            sbEmailBody.Append(absoluteUri + "/Account/ChangePassword?Id=" + uniqueId);
            mailmessage.IsBodyHtml = true;
            mailmessage.Body = sbEmailBody.ToString();
            mailmessage.Subject = "Reset Your Password";
            SmtpClient smtpclient = new SmtpClient("smtp.gmail.com", 587);
            smtpclient.Credentials = new System.Net.NetworkCredential()
            {
                UserName = ApplicationConstant.smtpMailSendingEmail,
                Password = ApplicationConstant.smtpMailSendingPassword
            };
            smtpclient.EnableSsl = true;
            try
            {
                smtpclient.Send(mailmessage);
            }
            catch (Exception)
            {
                throw;
            }
        }
        // end of private methods
        #endregion
    }
}