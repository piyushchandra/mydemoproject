﻿using System.Web.Mvc;

namespace BooksAdda.Controllers
{
    public class ErrorController : Controller
    {
        public ViewResult NotFound()
        {
            return View();
        }

        public ViewResult ErrorMessage()
        {
            return View();
        }
        public ViewResult NotAllowed()
        {
            return View();
        }
    }
}