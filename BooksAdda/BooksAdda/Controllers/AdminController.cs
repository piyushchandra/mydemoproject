﻿using BooksAdda.BusinessLayer;
using BooksAdda.CustomAttributes;
using BooksAdda.DefinedConstants;
using BooksAdda.Pagination;
using BooksAdda.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace BooksAdda.Controllers
{
    [CustomAuthorizeAttribute(allowedroles = ApplicationConstant.adminControllerAllowedRoles)]
    public class AdminController : Controller
    {
        public ViewResult AdminHomepage()
        {
            return View();
        }

        public ViewResult AddRole()
        {
            RoleViewModel model = new RoleViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ViewResult AddRole(RoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                AuthorizationRepository repo = new AuthorizationRepository();
                model.IsAdded = repo.AddRole(model);
            }
            return View(model);
        }

        public ViewResult DeleteRole()
        {
            AuthorizationRepository repo = new AuthorizationRepository();
            RoleViewModel model = new RoleViewModel();
            model.Roles = repo.GetRoleList().Select(x => x.RoleDescription).ToList();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ViewResult DeleteRole(RoleViewModel model)
        {
            AuthorizationRepository repo = new AuthorizationRepository();
            if (ModelState.IsValid)
            {
                model.IsDeleted = repo.DeleteRole(model);
            }
            model.Roles = repo.GetRoleList().Select(x => x.RoleDescription).ToList();
            return View(model);
        }

        private AssignRoleViewModel GetRolesInitialModel()
        {
            AuthorizationRepository repo = new AuthorizationRepository();
            UserRepository repo1 = new UserRepository();
            var model = new AssignRoleViewModel();
            var selectedRoles = new List<RoleViewModel>();
            model.AvailableRoles = repo.GetRoleList();
            model.SelectedRoles = selectedRoles;
            model.Emails = repo1.GetUserList().Select(x => x.Email).ToList();
            return model;
        }

        public ViewResult AssignRole()
        {
            return View(GetRolesInitialModel());
        }

        [HttpPost]
        public ViewResult AssignRole(AssignRoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                AuthorizationRepository repo = new AuthorizationRepository();
                var postedRoles = model.PostedRoles;
                var postedEmail = model.UserEmail;
                if (postedRoles != null)
                {
                    foreach (var RoleId in postedRoles.RoleIds)
                    {
                        model.IsAssigned = repo.AssignRole(RoleId, postedEmail);
                    }
                }
                return View(GetRolesModel(model, postedEmail));
            }
            return View(GetRolesInitialModel());
        }

        private AssignRoleViewModel GetRolesModel(AssignRoleViewModel model, string postedEmail)
        {
            AuthorizationRepository repo = new AuthorizationRepository();
            UserRepository repo1 = new UserRepository();
            PostedRoles postedRoles = model.PostedRoles;
            // setup properties
            var selectedRoles = new List<RoleViewModel>();
            var postedRoleIds = new List<int>();
            model.AvailableRoles = repo.GetRoleList();
            if (postedRoles == null) postedRoles = new PostedRoles();
            if (postedRoles.RoleIds != null && postedRoles.RoleIds.Any())
            {
                postedRoleIds = postedRoles.RoleIds;
            }
            // if there are any selected ids saved, create a list of roles
            if (postedRoleIds.Any())
            {
                selectedRoles = model.AvailableRoles
                 .Where(x => postedRoleIds.Any(s => x.RoleId.ToString().Equals(s.ToString())))
                 .ToList();
            }
            model.SelectedRoles = selectedRoles;
            model.PostedRoles = postedRoles;
            model.Emails = repo1.GetUserList().Select(x => x.Email).ToList();
            return model;
        }

        public ViewResult EditDeleteUser(int? page)
        {
            UserRepository repo = new UserRepository();
            UserListViewModel model = new UserListViewModel();
            model.Users = repo.GetUserList();
            model.RequestedPage = page;
            SetUserListPager(model);
            return View(model);
        }

        public ActionResult Delete(string email, int? page)
        {
            UserRepository repo = new UserRepository();
            repo.DeleteUser(email);
            return RedirectToAction("EditDeleteUser", new { page = page });
        }

        public ViewResult EditDeleteBook(int? page)
        {
            BooksRepository repo = new BooksRepository();
            BuyBooksViewModel model = new BuyBooksViewModel();
            model.Books = repo.GetBookListWithBookCategory();
            model.RequestedPage = page;
            SetBookListPager(model);
            return View(model);
        }

        private BuyBooksViewModel SetBookListPager(BuyBooksViewModel model)
        {
            var pager = new Pager(model.Books.Count(), model.RequestedPage);
            model.Books = model.Books.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize).ToList();
            model.Pager = pager;
            return model;
        }

        private UserListViewModel SetUserListPager(UserListViewModel model)
        {
            var pager = new Pager(model.Users.Count(), model.RequestedPage);
            model.Users = model.Users.Skip((pager.CurrentPage - 1) * pager.PageSize).Take(pager.PageSize).ToList();
            model.Pager = pager;
            return model;
        }

        public ViewResult BookEditById(int bookId)
        {
            BooksRepository repo = new BooksRepository();
            BookViewModel model = new BookViewModel();
            model = repo.GetBookById(bookId);
            model.Tags = repo.GetTagsByBookId(bookId);
            model.RelatedBooks = repo.GetRelatedBooks(bookId);
            return View(model);
        }

        public ActionResult DeleteBookById(int id, int? page)
        {
            BooksRepository repo = new BooksRepository();
            BuyBooksViewModel model = new BuyBooksViewModel();
            model.IsDeleted = repo.DeleteBookById(id);
            return RedirectToAction("EditDeleteBook", new { page = page });
        }

        public JsonResult GetBookTitleJson(string term)
        {
            BooksRepository repo = new BooksRepository();
            var titles = repo.GetBookTitles(term);
            return new JsonResult { Data = titles, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult DeleteRelatedBooks(int bookId, int[] relatedBookIds)
        {
            if (relatedBookIds != null)
            {
                BooksRepository repo = new BooksRepository();
                BookViewModel model = new BookViewModel();
                repo.DeleteRelatedBooks(bookId, relatedBookIds);
            }
            return Json(new { success = true });
        }

        public JsonResult DeleteTags(int bookId, string[] bookTags)
        {
            if (bookTags != null)
            {
                BooksRepository repo = new BooksRepository();
                BookViewModel model = new BookViewModel();
                repo.DeleteTags(bookId, bookTags);
            }
            return Json(new { success = true });
        }
    }
}