﻿using System.Web.Optimization;
namespace BooksAdda
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region
            // style bundle
            bundles.Add(new StyleBundle("~/Content/Autocomplete").Include(
                      "~/Content/jquery-ui.css",
                      "~/Content/jquery-ui.min.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/HomepageCssBundle").Include(
                      "~/Content/Homepage.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/HeaderAndFooter.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/AddBooks").Include(
                      "~/Content/AddBooks.css",
                      "~/Content/jquery-ui.css",
                      "~/Content/jquery-ui.min.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/ChangePassword").Include(
                      "~/Content/ChangePassword.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/AddRole").Include(
                      "~/Content/AddRole.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/AdminHomepage").Include(
                      "~/Content/AdminHomepage.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/EditBookById").Include(
                      "~/Content/EditBookById.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/DeleteRole").Include(
                      "~/Content/DeleteRole.css"
                      ));
            
            bundles.Add(new StyleBundle("~/Content/AssignRole").Include(
                      "~/Content/AssignRole.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/EditBooks").Include(
                      "~/Content/EditBooks.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/DeleteUser").Include(
                      "~/Content/DeleteUser.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/GetBookById").Include(
                      "~/Content/GetBookById.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/AddUser").Include(
                      "~/Content/AddUser.css"
                      ));
            bundles.Add(new StyleBundle("~/Content/EditUser").Include(
                      "~/Content/EditUser.css"
                      ));
            bundles.Add(new StyleBundle("~/Content/BuyBooks").Include(
                      "~/Content/BuyBooks.css"
                      ));
            //end of style bundle
            #endregion

            #region
            // script bundle
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                       "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                         "~/Scripts/jquery.js",
                         "~/Scripts/jquery-ui.min.js",
                         "~/Scripts/CustomScripts/ajax.serv.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/getbookbyid").Include(
                          "~/Scripts/CustomScripts/ajax.comment.js"
                         ));

            bundles.Add(new ScriptBundle("~/bundles/editbookbyid").Include(
                         "~/Scripts/CustomScripts/jquery.delete-related-book.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/HomepageBundle").Include(
                      "~/Scripts/CustomScripts/jquery.image-slideshow.js"));

            bundles.Add(new ScriptBundle("~/bundles/LayoutpageBundle").Include(
                      "~/Scripts/CustomScripts/jquery.backbutton.js"));
            //end of script bundle
            #endregion
        }
    }
}